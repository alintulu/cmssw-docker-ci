'''
author: adelina.eleonora.lintuluoto@cern.ch
date: 2020-06-02
updated: 2020-17-02

This script does the followig
    1) Initiate a GitLab object to connect to a GitLab server
    2) Get a list of the project issues that has a specific label
        a) Remove the label and add another indicating it is being processed
        b) Iterate the tagged issues and parse their description looking for a specific keyword
        c) Trigger a pipeline with the keyword specifing which job the GitLab CI should execute
'''

import sys
import os
import re
import argparse
import gitlab

regex_dict = {
    'base': re.compile(r'BASE_IMAGE: ([a-z A-Z0-9\\_\\"]+)'),
    'cmssw': re.compile(r'CMSSW_VERSION: [a-z A-Z0-9\\_\\"]+'),
    'scram': re.compile(r'SCRAM_ARCH: [a-z A-Z0-9\\_\\"]+'),
}

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--trigger", action="store_true", help="Trigger pipeline")
parser.add_argument("-ci", "--close-issue", type=int, help="Close issue, insert issue IID")
parser.add_argument("-ki", "--keep-issue", type=int, help="Keep issue open but change label, insert issue IID")
args = parser.parse_args()

class ConnectGitlab:

    def __init__(self):
        self.host = "".join(["https://", os.getenv("CI_SERVER_HOST")])
        self.project_path = os.getenv("CI_PROJECT_PATH")
        self.project_id = os.getenv("CI_PROJECT_ID")
        self.project_ref = os.getenv("CI_COMMIT_REF_NAME")
        self.private_token = os.getenv("PRIVATE_TOKEN")
        if not self.private_token:
            raise ValueError("Private token is undefined. Check Settings -> CI/CD -> Variables")
        self.trigger_token = os.getenv("TRIGGER_TOKEN")
        if not self.trigger_token:
            raise ValueError("Trigger token is undefined. Check Settings -> CI/CD -> Variables")
        self.connection = None
        
    def connect(self):
        self.connection = gitlab.Gitlab(self.host, private_token=self.private_token)
        self.connection.auth()

class TriggerPipeline:

    def __init__(self, ConnectGitlab):
        self.trigger_token = ConnectGitlab.trigger_token
        self.project_ref = ConnectGitlab.project_ref

    def parse_issue(self, issue):
        '''
        Remove and add label indicating issue is being handled. Create dictionary of input variables
        '''

        return self.parse_description(issue.description)

    def parse_description(self, line):
        '''
        Do a regex search and store the key and match of result
        '''

        input_dict = {}

        for key, regex in regex_dict.items():
            match = regex.search(line)
            if match:
                input_dict[key] = match.group(0).split(": ")[1].strip()

        return input_dict

    def trigger(self, input_dict, project, issue_iid):

        project.trigger_pipeline(self.project_ref, self.trigger_token, variables={'STANDALONE' : 'True', 'BASE_IMAGE' : input_dict['base'], 'CMSSW_VERSION' : input_dict['cmssw'], 'SCRAM_ARCH' : input_dict['scram'], 'ISSUE_IID' : issue_iid})

class HandleIssues:

    def __init__(self, project):
        self.project = project

    def create_editable_issue(self, issue_iid):
        editable_issue = self.project.issues.get(issue_iid, lazy=True)
        editable_issue.weight = None
        editable_issue.save()
        return editable_issue

    def add_label(self, editable_issue, label):
        editable_issue.labels.append(label)
        editable_issue.save()
    
    def remove_label(self, editable_issue, label):
        editable_issue.labels.remove(label)
        editable_issue.save()

    def close_issue(self, editable_issue):
        editable_issue.state_event = 'close'
        editable_issue.save()

try:
    gl = ConnectGitlab()
except ValueError as error:
    print(error)
    sys.exit(1)

# Create GitLab object with CI token authentication
gl.connect()
print("Connection to GitLab host established")

# Get the project by ID
try:
    project = gl.connection.projects.get(gl.project_id)
    print("Connected to project with ID ", gl.project_id)
except ValueError as error:
    print("Unable to connect to project with ID ", gl.project_id)
    sys.exit(1)

# Trigger pipeline
if args.trigger:
    tagged_issues = gl.connection.issues.list(state='opened', labels=['pending'])
    print("Found %d tagged issues\n"%len(tagged_issues))

    tp = TriggerPipeline(gl)
    hi = HandleIssues(project)

    for issue in tagged_issues:
        input_dict = tp.parse_issue(issue)
        print(input_dict)
        tp.trigger(input_dict, project, str(issue.iid))
        editable_issue = hi.create_editable_issue(issue.iid)

        hi.remove_label(editable_issue, 'pending')
        hi.add_label(editable_issue, 'building')

elif args.close_issue:

    hi = HandleIssues(project)

    issue_iid = args.close_issue
    editable_issue = hi.create_editable_issue(issue_iid)
    hi.remove_label(editable_issue, 'building')
    hi.close_issue(editable_issue)
    print("Closed issue with IID ", issue_iid, " from project with ID ", gl.project_id)

elif args.keep_issue:

    hi = HandleIssues(project)

    issue_iid = args.keep_issue
    editable_issue = hi.create_editable_issue(issue_iid)
    hi.remove_label(editable_issue, 'building')
    hi.add_label(editable_issue, 'error building')
    print("Kept issue open with IID ", issue_iid, " from project with ID ", gl.project_id)
